import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class LibraryService {

    constructor(private http: HttpClient) { }

    getReaders() {
        return this.http.get<LibraryReader[]>(`${environment.apiUrl}/reader`)
    }

    deleteReader(reader: LibraryReader) {
        return this.http.delete(`${environment.apiUrl}/reader/${reader.id}`)
    }

    putReader(reader: LibraryReader) {
        return this.http.put<LibraryReader>(`${environment.apiUrl}/reader/${reader.id}`, reader)
    }

    createReader(reader: LibraryReader) {
        return this.http.post<LibraryReader>(`${environment.apiUrl}/reader`, reader)
    }
}


export class LibraryReader {
    id: string = '';
    name: string = '';
    lastName: string = '';
    libraryName: string = '';
    libraryId: string = '';
}
