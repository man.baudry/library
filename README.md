# Library API

Manon Baudry

>**Ce projet ne correspond qu'à la partie Backend**,
la partie front a été réalisée en Angular.

## Réalisation
J'ai réaliser un CRUD pour les entités :
- Library
- Reader
- Author
- Comic
- Novel

## Architecture du code 
Le code est réparti dans 6 packages :

### models
- **Library** : Correspond à l'entité bibliothèque. 
  Elle contient des livres, des auteurs et des lecteurs.
- **Reader** : Correspond à un lecteur
- **Author** : Correspond à l'auteur d'un livre
- **Book** : Correspond à l'entité Livre
- **Comic** : Correspond à l'entité 'BD', elle hérite de l'entité Book
- **Novel** : Correspond à l'entité 'Roman', elle hérite de l'entité Book

### DTOS
Le package 'DTOs' regroupe les DTOs pour toutes les entités.

### mappers
Les classes Mappers permettent de transformer un modèle en son DTO, et inversement.

### repositories
Le package 'repositories' regroupe les repositories pour toutes les entités.

### services
Le package 'services' regroupe les services pour toutes les entités.
C'est dans ces classes que se trouve le code métier.

### controllers
Le package 'controllers' regroupe les controllers pour toutes les entités.

## Lancement du projet
Le projet peut être lancé avec la base embarquée H2 ou avec une base Derby
(cf les 2 fichiers applications.properties)

# Library Front

J'ai réalisé un client en Angular, pour lancer le projet:

`> npm install`

`> npm run start`

Vous pourrez ensuite consulter le site :
http://localhost:4200/
