package ari.library.DTOs;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ReaderDto {
    private Long id;
    private String name;
    private String lastName;
    private String libraryName;
    private Long libraryId;
}
