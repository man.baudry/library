package ari.library.DTOs;

import ari.library.models.Author;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class BookDto {
    private Long id;
    private String title;
    private String libraryName;
    private Long libraryId;
    private List<Long> authorIds;
    private List<String> authorNames;
}
