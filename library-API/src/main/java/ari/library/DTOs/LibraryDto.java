package ari.library.DTOs;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class LibraryDto {
    private Long id;
    private String name;
    private List<ReaderDto> readers;
    private List<AuthorDto> authors;
    private List<String> books;
}
