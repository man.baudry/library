package ari.library.DTOs;

import lombok.Data;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
public class NovelDto extends BookDto {

}
