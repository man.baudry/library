package ari.library.DTOs;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class AuthorDto {
    private Long id;
    private String name;
    private String lastName;
    private List<String> bookNames;
    private String libraryName;
    private Long libraryId;
}
