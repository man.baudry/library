package ari.library.services;

import ari.library.DTOs.LibraryDto;
import ari.library.mappers.LibraryMapper;
import ari.library.models.Library;
import ari.library.repositories.LibraryRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class LibraryService {

    @Autowired
    private LibraryRepository libraryRepository;

    /***
     * Create a new library
     * @param library the library to create
     * @return the library created
     */
    public LibraryDto createLibrary(LibraryDto library) {
        Library model = LibraryMapper.toModel(library);
        model = libraryRepository.save(model);
        return LibraryMapper.toDto(model);
    }

    /***
     *  Retrieve all the libraries
     * @return all the libraries
     */
    public List<LibraryDto> getAll() {
        List<Library> libraries = libraryRepository.findAll();
        List<LibraryDto> dtos = new ArrayList<>();
        libraries.forEach(x -> dtos.add(LibraryMapper.toDto(x)));
        return dtos;
    }


    /***
     *  Find a library by its id
     * @param id the id of the library to retrieve
     * @return the library DTO
     */
    public LibraryDto findById(Long id) {
        Optional<Library> model = libraryRepository.findById(id);

        if(!model.isPresent())
            throw new EntityNotFoundException();

        return LibraryMapper.toDto(model.get());
    }

    /***
     *  Delete a library by its id
     * @param id the id of the library to delete
     */
    public void deleteById(Long id) {
        if(!libraryRepository.existsById(id))
            throw new EntityNotFoundException();

        libraryRepository.deleteById(id);
    }

    /***
     * update a library's name
     * @param id the id of the library to update
     * @param dto the dto containing the fileds updated
     * @return the library updated
     */
    public LibraryDto updateById(Long id, LibraryDto dto) {
        Optional<Library> optionalLibrary = libraryRepository.findById(id);

        if(!optionalLibrary.isPresent())
            throw new EntityNotFoundException();

        Library model = optionalLibrary.get();
        model.setName(dto.getName());
        model = libraryRepository.save(model);
        return LibraryMapper.toDto(model);
    }

    /***
     * Find the library by its Id
     * @param id the library id
     * @return the library entity
     */
    public Library findEntityById(Long id) {
        Optional<Library> library = libraryRepository.findById(id);

        if(!library.isPresent())
            throw new EntityNotFoundException();
        return library.get();
    }

}
