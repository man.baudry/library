package ari.library.services;

import ari.library.DTOs.ReaderDto;
import ari.library.mappers.ReaderMapper;
import ari.library.models.Library;
import ari.library.models.Reader;
import ari.library.repositories.ReaderRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ReaderService {
    @Autowired
    private ReaderRepository readerRepository;

    @Autowired
    private LibraryService libraryService;

    /***
     * Create a new reader
     * @param reader the reader to create
     * @return the reader created
     */
    public ReaderDto createReader(ReaderDto reader) {
        Library library = libraryService.findEntityById(reader.getLibraryId());

        Reader model = ReaderMapper.toModel(reader, library);
        model = readerRepository.save(model);
        return ReaderMapper.toDto(model);
    }

    /***
     *  Retrieve all the readers
     * @return all the readers
     */
    public List<ReaderDto> getAll() {
        List<Reader> readers = readerRepository.findAll();
        List<ReaderDto> dtos = new ArrayList<>();
        readers.forEach(x -> dtos.add(ReaderMapper.toDto(x)));
        return dtos;
    }

    /***
     *  Find a reader by his id
     * @param id the id of the reader to retrieve
     * @return the Reader
     */
    public ReaderDto findById(Long id) {
        Optional<Reader> model = readerRepository.findById(id);

        if(!model.isPresent())
            throw new EntityNotFoundException();

        return ReaderMapper.toDto(model.get());
    }

    /***
     *  Delete a reader by his id
     * @param id the id of the reader to delete
     */
    public void deleteById(Long id) {
        if(!readerRepository.existsById(id))
            throw new EntityNotFoundException();

        readerRepository.deleteById(id);
    }

    /***
     * update a reader's informations
     * @param id the id of the reader to update
     * @param dto the dto containing the fileds updated
     * @return the reader updated
     */
    public ReaderDto updateById(Long id, ReaderDto dto) {
        Optional<Reader> optionalReader = readerRepository.findById(id);

        if(!optionalReader.isPresent())
            throw new EntityNotFoundException();

        Reader model = optionalReader.get();
        model.setName(dto.getName());
        model.setLastName(dto.getLastName());
        model.setLibrary(libraryService.findEntityById(dto.getLibraryId()));
        model = readerRepository.save(model);
        return ReaderMapper.toDto(model);
    }
}
