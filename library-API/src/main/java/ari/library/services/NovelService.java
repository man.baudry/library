package ari.library.services;

import ari.library.DTOs.NovelDto;
import ari.library.mappers.NovelMapper;
import ari.library.models.Author;
import ari.library.models.Comic;
import ari.library.models.Novel;
import ari.library.models.Library;
import ari.library.repositories.AuthorRepository;
import ari.library.repositories.NovelRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class NovelService {

    @Autowired
    private NovelRepository novelRepository;

    @Autowired
    private LibraryService libraryService;

    @Autowired
    private AuthorService authorService;

    @Autowired
    private AuthorRepository authorRepository;

    /***
     * Create a new novel
     * @param novel the novel to create
     * @return the novel created
     */
    public NovelDto createNovel(NovelDto novel) {
        Library library = libraryService.findEntityById(novel.getLibraryId());
        List<Author> authors = authorService.findEntitiesById(novel.getAuthorIds());

        Novel model = NovelMapper.toModel(novel, library, authors);
        model = novelRepository.save(model);
        return NovelMapper.toDto(model);
    }

    /***
     *  Retrieve all the novels
     * @return all the novels
     */
    public List<NovelDto> getAll() {
        List<Novel> novels = novelRepository.findAll();
        List<NovelDto> dtos = new ArrayList<>();
        novels.forEach(x -> dtos.add(NovelMapper.toDto(x)));
        return dtos;
    }

    /***
     *  Find a novel by his id
     * @param id the id of the novel to retrieve
     * @return the novel
     */
    public NovelDto findById(Long id) {
        Optional<Novel> model = novelRepository.findById(id);

        if(!model.isPresent())
            throw new EntityNotFoundException();

        return NovelMapper.toDto(model.get());
    }

    /***
     *  Delete a novel by his id
     * @param id the id of the novel to delete
     */
    public void deleteById(Long id) {
        if(!novelRepository.existsById(id))
            throw new EntityNotFoundException();

        Novel model = novelRepository.getById(id);
        model.getAuthors().forEach(x -> x.getBooks().remove(model));
        authorRepository.saveAll(model.getAuthors());
        novelRepository.deleteById(id);
    }

    /***
     * update a novel's personnal informations :  title
     * @param id the id of the novel to update
     * @param dto the dto containing the fileds updated
     * @return the novel updated
     */
    public NovelDto updateById(Long id, NovelDto dto) {
        Optional<Novel> optionalModel = novelRepository.findById(id);

        if(!optionalModel.isPresent())
            throw new EntityNotFoundException();

        Novel model = optionalModel.get();
        model.setTitle(dto.getTitle());
        model = novelRepository.save(model);
        return NovelMapper.toDto(model);
    }
}
