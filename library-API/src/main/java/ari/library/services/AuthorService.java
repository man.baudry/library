package ari.library.services;

import ari.library.DTOs.AuthorDto;
import ari.library.mappers.AuthorMapper;
import ari.library.models.Author;
import ari.library.models.Library;
import ari.library.repositories.AuthorRepository;
import ari.library.repositories.LibraryRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class AuthorService {
    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private LibraryService libraryService;

    @Autowired
    private LibraryRepository libraryRepository;

    /***
     * Create a new author
     * @param author the author to create
     * @return the author created
     */
    public AuthorDto createAuthor(AuthorDto author) {
        Library library = libraryService.findEntityById(author.getLibraryId());

        Author model = AuthorMapper.toModel(author, library);
        model = authorRepository.save(model);
        return AuthorMapper.toDto(model);
    }

    /***
     *  Retrieve all the authors
     * @return all the authors
     */
    public List<AuthorDto> getAll() {
        List<Author> authors = authorRepository.findAll();
        List<AuthorDto> dtos = new ArrayList<>();
        authors.forEach(x -> dtos.add(AuthorMapper.toDto(x)));
        return dtos;
    }

    /***
     *  Find a author by his id
     * @param id the id of the author to retrieve
     * @return the author
     */
    public AuthorDto findById(Long id) {
        Optional<Author> model = authorRepository.findById(id);

        if(!model.isPresent())
            throw new EntityNotFoundException();

        return AuthorMapper.toDto(model.get());
    }

    /***
     *  Delete a author by his id
     * @param id the id of the author to delete
     */
    public void deleteById(Long id) {
        if(!authorRepository.existsById(id))
            throw new EntityNotFoundException();

        Author model = authorRepository.findById(id).get();
        model.getLibrary().getReaders().remove(model);
        libraryRepository.save(model.getLibrary());
        authorRepository.deleteById(id);
    }

    /***
     * update a author's informations
     * @param id the id of the author to update
     * @param dto the dto containing the fileds updated
     * @return the author updated
     */
    public AuthorDto updateById(Long id, AuthorDto dto) {
        Optional<Author> optionalModel = authorRepository.findById(id);

        if(!optionalModel.isPresent())
            throw new EntityNotFoundException();

        Author model = optionalModel.get();
        model.setName(dto.getName());
        model.setLastName(dto.getLastName());
        model.setLibrary(libraryService.findEntityById(dto.getLibraryId()));

        model = authorRepository.save(model);
        return AuthorMapper.toDto(model);
    }

    /***
     * List the authors from their ids
     * @param authorIds the ids of the authors to retrieve
     * @return a list of the authors corresponding to the 'authorIds'
     */
    public List<Author> findEntitiesById(List<Long> authorIds) {
       return authorRepository.findAllById(authorIds);
    }
}
