package ari.library.services;

import ari.library.DTOs.ComicDto;
import ari.library.mappers.ComicMapper;
import ari.library.models.Author;
import ari.library.models.Comic;
import ari.library.models.Library;
import ari.library.repositories.AuthorRepository;
import ari.library.repositories.ComicRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ComicService {

    @Autowired
    private ComicRepository comicRepository;

    @Autowired
    private LibraryService libraryService;

    @Autowired
    private AuthorService authorService;

    @Autowired
    private AuthorRepository authorRepository;

    /***
     * Create a new comic
     * @param comic the comic to create
     * @return the comic created
     */
    public ComicDto createComic(ComicDto comic) {
        Library library = libraryService.findEntityById(comic.getLibraryId());
        List<Author> authors = authorService.findEntitiesById(comic.getAuthorIds());

        Comic model = ComicMapper.toModel(comic, library, authors);
        model = comicRepository.save(model);
        return ComicMapper.toDto(model);
    }

    /***
     *  Retrieve all the comics
     * @return all the comics
     */
    public List<ComicDto> getAll() {
        List<Comic> comics = comicRepository.findAll();
        List<ComicDto> dtos = new ArrayList<>();
        comics.forEach(x -> dtos.add(ComicMapper.toDto(x)));
        return dtos;
    }

    /***
     *  Find a comic by his id
     * @param id the id of the comic to retrieve
     * @return the comic
     */
    public ComicDto findById(Long id) {
        Optional<Comic> model = comicRepository.findById(id);

        if(!model.isPresent())
            throw new EntityNotFoundException();

        return ComicMapper.toDto(model.get());
    }

    /***
     *  Delete a comic by his id
     * @param id the id of the comic to delete
     */
    public void deleteById(Long id) {
        if(!comicRepository.existsById(id))
            throw new EntityNotFoundException();

        Comic model = comicRepository.getById(id);
        model.getAuthors().forEach(x -> x.getBooks().remove(model));
        authorRepository.saveAll(model.getAuthors());

        comicRepository.deleteById(id);
    }

    /***
     * update a comic's personnal informations : title
     * @param id the id of the comic to update
     * @param dto the dto containing the fileds updated
     * @return the comic updated
     */
    public ComicDto updateById(Long id, ComicDto dto) {
        Optional<Comic> optionalModel = comicRepository.findById(id);

        if(!optionalModel.isPresent())
            throw new EntityNotFoundException();

        Comic model = optionalModel.get();
        model.setTitle(dto.getTitle());
        model = comicRepository.save(model);
        return ComicMapper.toDto(model);
    }
}
