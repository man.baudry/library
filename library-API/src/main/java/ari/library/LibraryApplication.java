package ari.library;

import ari.library.models.*;
import ari.library.repositories.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;

@SpringBootApplication
public class LibraryApplication {

	public static void main(String[] args) {
		SpringApplication.run(LibraryApplication.class, args);
	}

	/***
	 * Seed database
	 * @param authorRepository
	 * @param novelRepository
	 * @param comicRepository
	 * @param libraryRepository
	 * @param readerRepository
	 * @return
	 */
	@Bean
	CommandLineRunner init(AuthorRepository authorRepository, NovelRepository novelRepository, ComicRepository comicRepository, LibraryRepository libraryRepository, ReaderRepository readerRepository) {
		return args -> {
			Library library = Library.builder()
					.name("ma bibliothèque")
					.build();
			library = libraryRepository.save(library);

			Library library2 = Library.builder()
					.name("Vieux Lille")
					.build();
			library2 = libraryRepository.save(library2);

			Author author = Author.builder()
					.name("Joël")
					.lastName("Dicker")
					.library(library)
					.build();
			author = authorRepository.save(author);

			Author authorComic = Author.builder()
					.name("Lee")
					.lastName("Stan")
					.library(library)
					.build();
			authorComic = authorRepository.save(authorComic);

			Novel novel =  Novel.builder()
					.title("La vérité sur l'affaire Harry Québert")
					.authors(Arrays.asList(author))
					.library(library)
					.build();
			novelRepository.save(novel);

			Comic comic = Comic.builder()
					.title("Spiderman")
					.series("first season")
					.authors(Arrays.asList(authorComic))
					.library(library)
					.build();
			comicRepository.save(comic);

			Reader reader = Reader.builder()
					.name("Manon")
					.lastName("Baudry")
					.library(library)
					.build();

			Reader reader2 = Reader.builder()
					.name("Jonathan")
					.lastName("Wadin")
					.library(library)
					.build();

			Reader reader3 = Reader.builder()
					.name("Tony")
					.lastName("Landschoot")
					.library(library)
					.build();

			Reader reader4 = Reader.builder()
					.name("Peter")
					.lastName("Parker")
					.library(library2)
					.build();

			library.setAuthors(Arrays.asList(author, authorComic));
			library.setBooks(Arrays.asList(novel, comic));
			library.setReaders(Arrays.asList(reader, reader2, reader3));
			libraryRepository.save(library);

			author.setBooks(Arrays.asList(novel));
			authorRepository.save(author);

			authorComic.setBooks(Arrays.asList(comic));
			authorRepository.save(authorComic);

			library2.setReaders(Arrays.asList(reader4));
			libraryRepository.save(library2);
		};
	}
}
