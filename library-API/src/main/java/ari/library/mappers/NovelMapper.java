package ari.library.mappers;

import ari.library.DTOs.NovelDto;
import ari.library.models.Author;
import ari.library.models.Novel;
import ari.library.models.Library;

import java.util.List;
import java.util.stream.Collectors;

/***
 * Converts the 'Novel' entity to dto and vice versa
 */
public class NovelMapper {
    /***
     * convert a Novel DTO to the corresponding entity
     * @param dto the DTO to convert
     * @param library the library associated to the novel
     * @param authors the authors of the book
     * @return the Novel model
     */
    public static Novel toModel(NovelDto dto, Library library, List<Author> authors){
        Novel model = Novel.builder()
                .id(dto.getId())
                .title(dto.getTitle())
                .library(library)
                .authors(authors)
                .build();

        return model;
    }

    /***
     * convert an entity Novel to a Novel DTO
     * @param model the model to convert
     * @return the Novel DTO
     */
    public static NovelDto toDto(Novel model){
        NovelDto dto = NovelDto.builder()
                .id(model.getId())
                .libraryName(model.getLibrary().getName())
                .libraryId(model.getLibrary().getId())
                .title(model.getTitle())
                .authorNames(model.getAuthors().stream().map(x->x.getName()).collect(Collectors.toList()))
                .authorIds(model.getAuthors().stream().map(x->x.getId()).collect(Collectors.toList()))
                .build();
        return dto;
    }
}
