package ari.library.mappers;

import ari.library.DTOs.AuthorDto;
import ari.library.models.Author;
import ari.library.models.Library;

import java.util.ArrayList;
import java.util.List;

/***
 * Converts the 'Author' entity to dto and vice versa
 */
public class AuthorMapper {

    /***
     * convert an Author DTO to the corresponding entity
     * @param dto the DTO to convert
     * @param library the library associated to the author
     * @return the Author model
     */
    public static Author toModel(AuthorDto dto, Library library){
        Author model = Author.builder()
                .id(dto.getId())
                .name(dto.getName())
                .lastName(dto.getLastName())
                .library(library)
                .build();

        return model;
    }

    /***
     * convert an entity Author to an Author DTO
     * @param model the model to convert
     * @return the author DTO
     */
    public static AuthorDto toDto(Author model){
        AuthorDto dto = AuthorDto.builder()
                .id(model.getId())
                .name(model.getName())
                .lastName(model.getLastName())
                .libraryId(model.getLibrary().getId())
                .libraryName(model.getLibrary().getName())
                .build();

        List<String> books = new ArrayList<>();
        model.getBooks().forEach(x -> books.add(x.getTitle()));
        dto.setBookNames(books);

        return dto;
    }
}
