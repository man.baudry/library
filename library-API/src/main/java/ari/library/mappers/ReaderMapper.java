package ari.library.mappers;

import ari.library.DTOs.ReaderDto;
import ari.library.models.Library;
import ari.library.models.Reader;

/***
 * Converts the 'Reader' entity to dto and vice versa
 */
public class ReaderMapper {

    /***
     * convert a Reader DTO to the corresponding entity
     * @param dto the DTO to convert
     * @param library the library associated to the reader
     * @return the Reader model
     */
    public static Reader toModel(ReaderDto dto, Library library){
        Reader model = Reader.builder()
                .id(dto.getId())
                .name(dto.getName())
                .lastName(dto.getLastName())
                .library(library)
                .build();
        return model;
    }

    /***
     * convert an entity Reader to a Reader DTO
     * @param model the model to convert
     * @return the Reader DTO
     */
    public static ReaderDto toDto(Reader model){
        ReaderDto dto =  ReaderDto.builder()
                .id(model.getId())
                .name(model.getName())
                .lastName(model.getLastName())
                .libraryId(model.getLibrary().getId())
                .libraryName(model.getLibrary().getName())
                .build();
        return dto;
    }
}
