package ari.library.mappers;

import ari.library.DTOs.ComicDto;
import ari.library.models.Author;
import ari.library.models.Comic;
import ari.library.models.Library;

import java.util.List;
import java.util.stream.Collectors;

/***
 * Converts the 'Comic' entity to dto and vice versa
 */
public class ComicMapper {
    /***
     * convert a Comic DTO to the corresponding entity
     * @param dto the DTO to convert
     * @param library the library associated to the comic
     * @param authors the authors of the book
     * @return the Comic model
     */
    public static Comic toModel(ComicDto dto, Library library, List<Author> authors){
        Comic model = Comic.builder()
                .id(dto.getId())
                .title(dto.getTitle())
                .library(library)
                .authors(authors)
                .series(dto.getSeries())
                .build();

        return model;
    }

    /***
     * convert an entity Comic to a Comic DTO
     * @param model the model to convert
     * @return the Comic DTO
     */
    public static ComicDto toDto(Comic model){
        ComicDto dto = ComicDto.builder()
                .id(model.getId())
                .libraryName(model.getLibrary().getName())
                .libraryId(model.getLibrary().getId())
                .title(model.getTitle())
                .series(model.getSeries())
                .authorNames(model.getAuthors().stream().map(x->x.getName()).collect(Collectors.toList()))
                .authorIds(model.getAuthors().stream().map(x->x.getId()).collect(Collectors.toList()))
                .build();
        return dto;
    }
}
