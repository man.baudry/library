package ari.library.mappers;

import ari.library.DTOs.AuthorDto;
import ari.library.DTOs.LibraryDto;
import ari.library.DTOs.ReaderDto;
import ari.library.models.Author;
import ari.library.models.Library;
import ari.library.models.Reader;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/***
 * Converts the 'Library' entity to dto and vice versa
 */
public class LibraryMapper {

    /***
     * convert a Library DTO to the corresponding entity
     * @param dto the DTO to convert
     * @return the Library model
     */
    public static Library toModel(LibraryDto dto){
        Library model = Library.builder()
                .id(dto.getId())
                .name(dto.getName())
                .build();

        List<Reader> readers = new ArrayList<>();
        dto.getReaders().forEach(x -> readers.add(ReaderMapper.toModel(x, model)));
        model.setReaders(readers);

        List<Author> authors = new ArrayList<>();
        dto.getAuthors().forEach(x -> authors.add(AuthorMapper.toModel(x, model)));
        model.setAuthors(authors);

        return model;
    }

    /***
     * convert an entity Library to a Library DTO
     * @param model the model to convert
     * @return the Library DTO
     */
    public static LibraryDto toDto(Library model){
        LibraryDto dto =  LibraryDto.builder()
                .id(model.getId())
                .name(model.getName())
                .readers(model.getReaders().stream().map(x->ReaderMapper.toDto(x)).collect(Collectors.toList()))
                .authors(model.getAuthors().stream().map(x->AuthorMapper.toDto(x)).collect(Collectors.toList()))
                .books(model.getBooks().stream().map(x-> x.getTitle()).collect(Collectors.toList()))
                .build();
        return dto;
    }
}
