package ari.library.controllers;

import ari.library.DTOs.ComicDto;
import ari.library.services.ComicService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/comic")
public class ComicController {

    @Autowired
    private ComicService comicService;

    @CrossOrigin
    @PostMapping
    public ComicDto createComic(@RequestBody ComicDto dto){
        return comicService.createComic(dto);
    }

    @CrossOrigin
    @GetMapping()
    public List<ComicDto> getAll(){
        return comicService.getAll();
    }

    @CrossOrigin
    @GetMapping("/{id}")
    public ComicDto findById(@PathVariable("id") Long id){
        return comicService.findById(id);
    }

    @CrossOrigin
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") Long id){
        comicService.deleteById(id);
    }

    @CrossOrigin
    @PutMapping("/{id}")
    public ComicDto updateById(@PathVariable("id") Long id, @RequestBody ComicDto dto){
        return comicService.updateById(id, dto);
    }
}
