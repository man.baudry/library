package ari.library.controllers;

import ari.library.DTOs.ReaderDto;
import ari.library.services.ReaderService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/reader")
public class ReaderController {

    @Autowired
    ReaderService readerService;

    @CrossOrigin
    @PostMapping
    public ReaderDto createReader(@RequestBody ReaderDto reader){
        return readerService.createReader(reader);
    }

    @CrossOrigin
    @GetMapping()
    public List<ReaderDto> getAll(){
        return readerService.getAll();
    }

    @CrossOrigin
    @GetMapping("/{id}")
    public ReaderDto findById(@PathVariable("id") Long id){
        return readerService.findById(id);
    }

    @CrossOrigin
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") Long id){
        readerService.deleteById(id);
    }

    @CrossOrigin
    @PutMapping("/{id}")
    public ReaderDto updateById(@PathVariable("id") Long id, @RequestBody ReaderDto dto){
        return readerService.updateById(id, dto);
    }
}
