package ari.library.controllers;

import ari.library.DTOs.AuthorDto;
import ari.library.services.AuthorService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/author")
public class AuthorController {
    @Autowired
    AuthorService authorService;

    @CrossOrigin
    @PostMapping
    public AuthorDto createAuthor(@RequestBody AuthorDto author){
        return authorService.createAuthor(author);
    }

    @CrossOrigin
    @GetMapping()
    public List<AuthorDto> getAll(){
        return authorService.getAll();
    }

    @CrossOrigin
    @GetMapping("/{id}")
    public AuthorDto findById(@PathVariable("id") Long id){
        return authorService.findById(id);
    }

    @CrossOrigin
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") Long id){
        authorService.deleteById(id);
    }

    @CrossOrigin
    @PutMapping("/{id}")
    public AuthorDto updateById(@PathVariable("id") Long id, @RequestBody AuthorDto dto){
        return authorService.updateById(id, dto);
    }
}
