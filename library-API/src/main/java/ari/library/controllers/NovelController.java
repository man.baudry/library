package ari.library.controllers;

import ari.library.DTOs.NovelDto;
import ari.library.services.NovelService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/novel")
public class NovelController {
    @Autowired
    private NovelService novelService;

    @CrossOrigin
    @PostMapping
    public NovelDto createNovel(@RequestBody NovelDto dto){
        return novelService.createNovel(dto);
    }

    @CrossOrigin
    @GetMapping()
    public List<NovelDto> getAll(){
        return novelService.getAll();
    }

    @CrossOrigin
    @GetMapping("/{id}")
    public NovelDto findById(@PathVariable("id") Long id){
        return novelService.findById(id);
    }

    @CrossOrigin
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") Long id){
        novelService.deleteById(id);
    }

    @CrossOrigin
    @PutMapping("/{id}")
    public NovelDto updateById(@PathVariable("id") Long id, @RequestBody NovelDto dto){
        return novelService.updateById(id, dto);
    }
}
