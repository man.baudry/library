package ari.library.controllers;

import ari.library.DTOs.LibraryDto;
import ari.library.services.LibraryService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/library")
public class LibraryController {

    @Autowired
    LibraryService libraryService;

    @CrossOrigin
    @PostMapping
    public LibraryDto createLibrary(@RequestBody LibraryDto library){
        return libraryService.createLibrary(library);
    }

    @CrossOrigin
    @GetMapping()
    public List<LibraryDto> getAll(){
        return libraryService.getAll();
    }

    @CrossOrigin
    @GetMapping("/{id}")
    public LibraryDto findById(@PathVariable("id") Long id){
        return libraryService.findById(id);
    }

    @CrossOrigin
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") Long id){
        libraryService.deleteById(id);
    }

    @CrossOrigin
    @PutMapping("/{id}")
    public LibraryDto updateById(@PathVariable("id") Long id, @RequestBody LibraryDto dto){
        return libraryService.updateById(id, dto);
    }
}
