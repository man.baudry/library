package ari.library.models;

import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Builder
@Data
public class Reader {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @Size(min=2, max=30)
    private String name;

    @NonNull
    @Size(min=2, max=30)
    private String lastName;

    @ManyToOne
    private Library library;

    public Reader() { }

    public Reader(Long id, @NonNull String name, @NonNull String lastName, Library library) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.library = library;
    }
}
