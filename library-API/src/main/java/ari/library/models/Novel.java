package ari.library.models;

import lombok.*;
import lombok.experimental.SuperBuilder;
import net.bytebuddy.implementation.bind.annotation.Super;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import java.util.List;

/***
 * Représente l'entité Roman
 */
@Entity
@Inheritance(strategy= InheritanceType.TABLE_PER_CLASS)
@SuperBuilder
@Data
public class Novel extends Book {

    public Novel() {
        super();
    }

    public Novel(Long id, @NonNull String title, List<Author> author, Library library) {
        super(id, title, author, library);
    }
}
