package ari.library.models;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Builder
@Data
public class Library {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @Size(min=2, max=30)
    private String name;

    @OneToMany(mappedBy = "library", cascade = CascadeType.ALL)
    private List<Reader> readers;

    @OneToMany(mappedBy = "library", cascade = CascadeType.ALL)
    private List<Author> authors;

    @OneToMany(mappedBy = "library", cascade = CascadeType.ALL)
    private List<Book> books;

    public Library() { }

    public Library(Long id, @NonNull String name, List<Reader> readers, List<Author> authors, List<Book> books) {
        this.id = id;
        this.name = name;
        this.readers = readers;
        this.authors = authors;
        this.books = books;
    }
}
