package ari.library.models;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

/***
 * Représente l'entité Livre
 */
@Entity
@Data
@SuperBuilder
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @Size(min=1, max=100)
    private String title;

    @ManyToMany
    private List<Author> authors;

    @ManyToOne
    private Library library;

    public Book() { }

    public Book(Long id, @NonNull String title, List<Author> authors, Library library) {
        this.id = id;
        this.title = title;
        this.authors = authors;
        this.library = library;
    }
}
