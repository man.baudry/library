package ari.library.models;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.validation.constraints.Size;
import java.util.List;

/***
 * Représente l'entité BD
 */
@Entity
@SuperBuilder
@Data
@Inheritance(strategy= InheritanceType.TABLE_PER_CLASS)
public class Comic extends Book {
    @NonNull
    @Size(min=1, max=100)
    private String series;

    public Comic() { }

    public Comic(@NonNull String series) {
        this.series = series;
    }

    public Comic(Long id, @NonNull String title, List<Author> author, Library library, @NonNull String series) {
        super(id, title, author, library);
        this.series = series;
    }
}
