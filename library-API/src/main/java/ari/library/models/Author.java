package ari.library.models;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Data
@Builder
public class Author {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @Size(min=2, max=30)
    private String name;

    @NonNull
    @Size(min=2, max=30)
    private String lastName;

    @ManyToMany
    private List<Book> books;

    @ManyToOne
    private Library library;

    public Author()  {}

    public Author(Long id, @NonNull String name, @NonNull String lastName, List<Book> books, Library library) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.books = books;
        this.library = library;
    }
}
